<?php

/**
 * @file
 * Install, update and uninstall functions for the Email Notifications module.
 */

/**
 * Implements hook_install().
 */
function email_notifications_install() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_uninstall().
 */
function email_notifications_uninstall() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_schema().
 */
function email_notifications_schema() {
  $schema['notification'] = [
    'description' => 'Email notifications log table.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique record ID.',
      ],
      'uuid' => [
        'type' => 'varchar',
        'length' => 64,
        'description' => 'The universally unique identifier.',
      ],
      'label' => [
        'description' => 'The notification label.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp when the record was created.',
      ],
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid of the user who created the record.',
      ],
      'to' => [
        'description' => 'The email address of the primary recipient.',
        'type' => 'varchar',
        'length' => 255,
      ],
      'cc' => [
        'description' => 'The email addresses of the CC recipients.',
        'type' => 'varchar',
        'length' => 255,
      ],
      'bcc' => [
        'description' => 'The email addresses of the BCC recipients.',
        'type' => 'varchar',
        'length' => 255,
      ],
      'subject' => [
        'description' => 'The subject of the email.',
        'type' => 'varchar',
        'length' => 255,
      ],
      'body' => [
        'description' => 'The body content of the email.',
        'type' => 'text',
      ],
      'timestamp' => [
        'description' => 'The timestamp when the email was logged.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Boolean indicating whether this record is active.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
      'type' => [
        'type' => 'varchar_ascii',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Type of the record.',
      ],
      'changed' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp when the record was changed.',
      ],
      'data' => [
        'type' => 'blob',
        'size' => 'big',
        'description' => 'The arbitrary data for the item.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'type' => ['type'],
      'uid' => ['uid'],
      'status' => ['status'],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function email_notifications_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $value = mt_rand(0, 100);
    $requirements['email_notifications_status'] = [
      'title' => t('Email Notifications status'),
      'value' => t('Email Notifications value: @value', ['@value' => $value]),
      'severity' => $value > 50 ? REQUIREMENT_INFO : REQUIREMENT_WARNING,
    ];
  }

  return $requirements;
}
