<?php

namespace Drupal\email_notifications\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for the email_notifications module.
 */
class EmailNotificationsController extends ControllerBase {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * EmailNotificationsController constructor.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(MailManagerInterface $mailManager, EntityTypeManagerInterface $entity_type_manager) {
    $this->mailManager = $mailManager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Resends an email.
   *
   * @param int $log_id
   *   The log id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response back to the original page.
   */
  public function resendEmail($log_id) {
    $notification = $this->entityTypeManager()->getStorage('notification')->load($log_id);
    $to = $notification->to->value;
    $cc = $notification->cc->value;
    $bcc = $notification->bcc->value;
    $mailManager = $this->mailManager;
    $module = 'email_notifications';
    $key = 'resend';
    $params = [
      'subject' => $notification->subject->value,
      'message' => $notification->body->value,
    ];
    $cc ? $params['headers']['Cc'] = $cc : '';
    $bcc ? $params['headers']['Bcc'] = $bcc : '';
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

    $messenger = $this->messenger();
    if ($result['result'] === TRUE) {
      $messenger->addStatus(t('The email has been resent successfully.'));
    }
    else {
      $messenger->addError(t('An error occurred while resending the email.'));
    }
    $uri = \Drupal::request()->query->get('destination', '/');
    return new RedirectResponse($uri);
  }

}
