<?php

namespace Drupal\email_notifications\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the notification entity edit forms.
 */
class NotificationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New notification %label has been created.', $message_arguments));
        $this->logger('email_notifications')->notice('Created new notification %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The notification %label has been updated.', $message_arguments));
        $this->logger('email_notifications')->notice('Updated notification %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.notification.canonical', ['notification' => $entity->id()]);

    return $result;
  }

}
