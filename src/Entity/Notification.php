<?php

namespace Drupal\email_notifications\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\email_notifications\NotificationInterface;

/**
 * Defines the notification entity class.
 *
 * @ContentEntityType(
 *   id = "notification",
 *   label = @Translation("Notification"),
 *   label_collection = @Translation("Notifications"),
 *   label_singular = @Translation("notification"),
 *   label_plural = @Translation("notifications"),
 *   label_count = @PluralTranslation(
 *     singular = "@count notifications",
 *     plural = "@count notifications",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\email_notifications\NotificationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\email_notifications\Form\NotificationForm",
 *       "edit" = "Drupal\email_notifications\Form\NotificationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "notification",
 *   admin_permission = "administer notification",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/notification",
 *     "add-form" = "/notification/add",
 *     "canonical" = "/notification/{notification}",
 *     "edit-form" = "/notification/{notification}/edit",
 *     "delete-form" = "/notification/{notification}/delete",
 *   },
 *   field_ui_base_route = "entity.notification.settings",
 * )
 */
class Notification extends ContentEntityBase implements NotificationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the notification was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the notification was last edited.'));

    $fields['to'] = BaseFieldDefinition::create('string')
      ->setLabel(t('To'))
      ->setDescription(t('The email address of the primary recipient.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'email',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'email',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['cc'] = BaseFieldDefinition::create('string')
      ->setLabel(t('CC'))
      ->setDescription(t('The email addresses of the CC recipients.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['bcc'] = BaseFieldDefinition::create('string')
      ->setLabel(t('BCC'))
      ->setDescription(t('The email BCC.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subject'))
      ->setDescription(t('The subject of the email.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['body'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Body'))
      ->setDescription(t('The body content of the email.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['timestamp'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Timestamp'))
      ->setDescription(t('The timestamp when the email was logged.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'timestamp',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Logs the email before it is sent.
   */
  public static function logEmail(array $message) {

    $cc = $message['headers']['Cc'] ?? [];
    if (!is_array($cc)) {
      $cc = array_map('trim', explode(',', $cc));
    }
    $bcc = $message['headers']['Bcc'] ?? [];
    if (!is_array($bcc)) {
      $bcc = array_map('trim', explode(',', $bcc));
    }
    $notification = self::create([
      'label' => $message['to'] . ' - ' . $message['subject'],
      'to' => $message['to'],
      'cc' => $cc,
      'bcc' => $bcc,
      'subject' => $message['params']['subject'] ?? $message['subject'],
      'body' => $message['params']['message'] ?? $message['body'][0],
      'timestamp' => time(),
    ]);
    try {
      $notification->save();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

}
