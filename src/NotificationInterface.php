<?php

namespace Drupal\email_notifications;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a notification entity type.
 */
interface NotificationInterface extends ContentEntityInterface, EntityChangedInterface {

}
